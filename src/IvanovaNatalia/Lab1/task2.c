#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int main()

{
    int hour, minut;
    char greeting[][40] = { "Good morning!", "Good day!", "Good everning!", "Good night!"};

    printf ("Please, enter hour, minut! On form: hh.mm\n");
    if (scanf("%d.%d", &hour, &minut) == 2)
    {
        if ((hour >= 0 && hour <= 23) && (minut >= 0 && minut <= 59))
            printf("%s\n", greeting[hour > 4 && hour < 11 ? 0 : hour > 11 && hour < 17 ? 1 : hour > 17 && hour < 21 ? 2 : 3]);
        else 
            puts("Input error!");
    } 
    else 
        puts("Error!");

    return 0;
}
